# Sample Code: Rotoscope

This is sample code for an article, that demonstrates how to take
a Python project from wrinkly script to full blown CLI tool,
increasing the quality on the way.

# Terms

Written by Mark Meyer (mark@ofosos.org)

Copyright (c) 2022, Mark Meyer

Licensed under the Good Luck With That Public License, see LICENSE.txt for details.

# Coffee

<a href="https://www.buymeacoffee.com/markZb" target="_blank"><img src="https://cdn.buymeacoffee.com/buttons/v2/default-yellow.png" alt="Buy Me A Coffee" style="height: 30px !important;width: 109px !important;" ></a>
