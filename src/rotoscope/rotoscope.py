#!/usr/bin/python

from datetime import datetime
from glob import glob
from os import link, unlink
from os.path import basename, exists, join
from shutil import move

import click

LATEST = "latest.txt"
TPATTERN = "%Y-%m-%d"


def transmogrify_filename(fname):
    bname = basename(fname)
    ts = datetime.now().strftime(TPATTERN)
    return "-".join([ts, bname])


def set_current_latest(archive, file):
    latest = join(archive, LATEST)
    if exists(latest):
        unlink(latest)
    link(file, latest)


def rotate_file(source, archive):
    target = join(archive, transmogrify_filename(source))
    move(source, target)
    set_current_latest(archive, target)


@click.command()
@click.option(
    "archive",
    "--archive",
    default="/Users/mark/archive",
    envvar="ROTO_ARCHIVE",
    type=click.Path(),
)
@click.argument("incoming", type=click.Path(exists=True))
def rotoscope(incoming, archive):
    """
    Rotoscope 0.5 - Bleep, blooop.
    Simple sample that move files.
    """
    file_no = 0
    folder = join(incoming, "*.txt")
    print(f"Looking in {incoming}")
    for file in glob(folder):
        rotate_file(file, archive)
        print(f"Rotated: {file}")
        file_no = file_no + 1

    print(f"Total files rotated: {file_no}")


if __name__ == "__main__":
    rotoscope()
